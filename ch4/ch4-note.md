#WORKING WITH BASIC CLASSES AND VARIABLES

##CERTIFICATION OBJECTIVE 13: Understand Primitives, Enumerations, and Objects

###Exam Objective
- declare and initialize variables (including casting of primitive data types)
- develop code that uses wrapper classes such as Boolean, Double and Integer
- differentiate between object reference variables and primitive variables

###Learning Outcome
- primitive variables store basic data
- primitives are used in objects
- object-oriented : it allows the developper to organize related code and data together in discrete objects

####13.1 Primitive Variables
- 8 primitives : store what kind of value? size in memory? limit size?

#####13.1.1 What is a Primitive?
- the most basic form of data
- 8 primitive data types are : boolean, char, byte, short, int, long, float, double
- you may only set it or read it
- calculations performed with primitives are much faster than similar objects

#####1.13.2 Declaring and Initializing Primitives
- boolean primitives as instance varialbes get assigned a default value of 0 or false
- they must be assigned a value before being used

#####1.13.3 boolean Primitive
- true or false
- 1-bit, default to false as an instance variable
- exact size is not defined in the Java standard and may occupy more space on different platforms

#####1.13.4 char Primitive
- single 16-bit Unicode character
- requires 16 bits of memory
- range : '\u0000' (or 0) and '\uffff' (or 65535 inclusive)
- single quote should be used
- dafault value of '\u0000' or 0 as an instance variable
- always unsigned
![char](img/char.jpg)

#####1.13.5 byte Primitive
- store small, signed numbers up to 1 byte in size
- default value 0
- occupy 8 bits, max 127 min -128 inclusive
- implicite case & explicit case

#####1.13.6 short Primitive
- default 0
- 16 bit, signed, max 32767, min -32768
- short a = 2350 // implicit cast
- short b = (short) 427 // explicit cast

#####1.13.7 int Primitive
- default 0
- 32 bit signed, max 2147483647, min -2147483648

#####1.13.8 long Primitive







***
##Annexe

###Reading list
- [Java Operators](http://docs.oracle.com/javase/tutorial/java/nutsandbolts/operators.html)