public class PrimitiveTest{

  public static void main(String[] args){
    int auctionPrice = 7800000;
    System.out.println(auctionPrice);
    char cylinders = '\u0008';
    System.out.println(cylinders);
    int cyl = cylinders; // implicit cast from char to integer
    System.out.println(cyl);
    byte wheelbase = 90;
    int wBase = wheelbase; // implicit cast from byte to integer
    System.out.println(wBase);
    short horsepower = 250;
    int hPower = horsepower; // implicit cast from short to integer
    System.out.println(hPower);
    int length = (int) 151.5F; // floats must be explicitly casted
    System.out.println(length);
    int powerToWeightRatio = (int) 405.1D; // doubles must be explicitly casted   
    System.out.println(powerToWeightRatio);
  }
}
