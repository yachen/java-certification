#PROGRAMMING WITH JAVA OPERATORS AND STRINGS

##CERTIFICATION OBJECTIVE 9: Understand Fundamental Operators

###Exam Objective 
- Use Java operators, incluing parentheses, to override operator precedence.

###Learning Objective

####9.1 Assignment Operators
- =
- cause compiler errors when invalid type

####9.2 Compound Assignment Operators
- +=, -=
- it's good practice to use "longhand" approach in order to improve readability
- exercise : Using Compound Assignment Operators

Assigned Value of a | Compound Assignment | Refactored Statement | New Value of a   
 ------------------ | ------------------- | ----------------- | ---------------- 
a = 10; | a += 3; | a = 10 + 3; | 13
a = 15; | a -= 3; | a = 15 - 3; | 12
a = 20; | a *= 3; | a = 20 * 3; | 60
a = 25; | a /= 3; | a = 25 / 3; | 8
a = 30; | a %= 3; | a = 30 % 3; | 0
a = 35; | a &= 3; | a = 35 & 3; | 3 
a = 40; | a ^= 3; | a = 40 ^ 3; | 43
a = 45; | a \|= 3; | a = 45 \| 3; | 47  
a = 50; | a <<= 3; | a = 50 << 3; | -112
a = 55; | a >>= 3; | a = 55 >> 3; | 6 
a = 60; | a >>>= 3; | a = 60 >>> 3; | 7

####9.3 Arithmetic Operators
- Basic Arithmetic Operators : +,-,*,/,%
- Prefix-Increment, Postfix-Increment, Prefix-Decrement, and Postfix-Decrement Operators : ++x, --x, x++, x--
- y = ++x equals x=x+1; y=x;
- y = x++ equals y=x; x=x+1;

####9.4 Relational Operators
- basic relational operators : <, <=, >, >=
- used to compare integers, floating points, and characters (unicode /u0041, hexadecimal 0x0041, octal 0101 or '\101')
- equality operators : ==, !=
- You should actually use an epsilon when comparing floating-point numbers
- Numeric Promotion of Binary Values
![numeric_promotion](img/numeric_promotion.jpg)

####9.5 Logical Operators
- Logical (Conditional) Operators : &&, ||
- for AND, whenever the left operand returns false, the expression terminates and returns a value of false.
- for OR, whenever the left operand returns true, the expression terminates and returns a value of true.
- Logical Negation Operator : !
![scenario](img/scenario.jpg)

####9.4 Understand Operator Precedence
![precedence](img/precedence.jpg)

- Use parentheses to override operator precedence

##CERTIFICATION OBJECTIVE 10: Use String Objects and Their Methods

###Exam Objective
- Create and manipulate strings

###Learning Outcome

####10.1 Strings
- 16-bit Unicode character strings
- "Y" = \u0059 = 000001011001
- "o" = \u006F = 000001101111
- String exclamation = "Yo"; // 000001011001 and 000001101111
- Strings are immutable objects, meaning their values never change
- a reference variable holds the value's address
![string_scenario](img/string_scenario.jpg)
- if you want to use a mutable character string, consider StringBuffer or StringBuilder

####10.2 The String Concatenation Operator
- "+" sign, left-associative

####10.3 The toString Method
- All classes in Java extend the Object class by default; therefore, every class inherits this method. 
- Exercise : Uncovering Bugs that Your Compiler May Not Find
<pre><code>String s1 = new String ("String one");
// Dead store to local variable 本地变量存储了闲置不用的对象</code></pre>

####10.4 Methods of the String Class
<pre><code>String pirateMessage = "  Buried Treasure Chest!"</code></pre>
![index.jpg](img/index.jpg)

#####10.4.1 The charAt Method
<pre><code>char c1 = pirateMessage.charAt(2); // returns 'B'</code></pre>

#####10.4.2 The indexOf Method
<pre><code>int i1 = pirateMessage.indexOf('u'); // returns 3
int i2 = pirateMessage.indexOf('u',9); // returns 14
int i3 = pirateMessage.indexOf("sure", 13); // returns -1
int i4 = pirateMessage.indexOf("Treasure", 10); // returns -1
int i5 = pirateMessage.indexOf("u", 100); // returns -1</code></pre>

#####10.4.3 The length Method
<pre><code>int i = pirateMessage.length(); // returns 25</code></pre>
- string .length()
- array .length
- 
#####10.4.4 The concat Method
<pre><code>String c = pirateMessage.concat("Weigh anchor!"); 
// returns "  Buried Treasure Chest! Weigh anchor!"</code></pre>

#####10.4.5 The replace Method
- CharSequence interface allows for the use of either a *String, StringBuffer or StringBuilder*
<pre><code>String s1 = pirateMessage.replace('B', 'J'); 
// returns "  Juried Treasure Chest! "
String s2 = pirateMessage.replace(' ', 'X'); 
// returns "XXBuriedXTreasure Chest! "
String s3 = pirateMessage.replace("Chest", "Coins"); 
// returns "  Buried Treasure Coins! "</code></pre>

#####10.4.6 The startsWith Method
<pre><code>boolean b1 = pirateMessage.startsWith("  Buried Treasure"); 
// returns true
boolean b2 = pirateMessage.startsWith("Treasure", 8); 
// returns false</code></pre>

#####10.4.6 The endsWith Method
<pre><code>boolean e1 = pirateMessage.endsWith("Treasure Chest! "); 
// returns true</code></pre>

#####10.4.7 The substring Method
<pre><code>String ss1 = pirateMessage.substring(9); 
// returns "Treasure Chest! "
String ss2 = pirateMessage.substring(9, 10); 
// returns "T"
String ss3 = pirateMessage.substring(9, 9); 
// returns blank
String ss4 = pirateMessage.substring(9, 8); 
// out of range</code></pre>

#####10.4.8 The trim Method
- whitespace " " = unicode \u0020
<pre><code>String t = pirateMessage.trim(); 
// returns "Buried Treasure Chest!"</code></pre>

#####10.4.9 The toLowerCase Method
<pre><code>String l1 = pirateMessage.toLowerCase(); 
// returns " buried trearsure chest! "</code></pre>

#####10.4.10 The toUpperCase Method
<pre><code>String l1 = pirateMessage.toUpperCase(); 
// returns " BURIED TREATURE CHEST! "</code></pre>

#####10.4.11 Chaining
- Whether methods are invoked separately or chained together, the end result is the same

#####10.4.12 The equalsIgnoreCase Method
<pre><code>boolean b1 = pirateMessage.equalsIgnoreCase("  Buried TREASURE Chest! "); 
// returns true</code></pre>

##CERTIFICATION OBJECTIVE 11: Use StringBuilder Objects and Their Methods

###Exam Objective
- manipulate data using the StringBuilder class and its methods

###Learning Outcome
- StringBuilder class : mutable
- StringBuffer class : thread-safe mutable
- String : immutable

####11.1 Methods of the StringBuilder Class
<pre><code>StringBuilder mateyMessage = new StringBuilder ("Shiver Me Timbers!")</code></pre>
![string_builder.jpg](img/string_builder.jpg)

#####11.1.1 The append Method
- 13 method declarations, provide coverage of the different Java types
<pre><code>StringBuilder e = new StringBuilder ("Examples:")
e.append(" ").append((int) 2);
// return "Examples: 2"</code></pre>

#####11.1.2 The insert Method
<pre><code>StringBuilder e = new StringBuilder ("Shiver Me Timbers");
e.insert(17, " and Bricks");
// return "Shiver Me Timbers and Bricks"</code></pre>

#####11.1.3 The delete Method
<pre><code>StringBuilder e = new StringBuilder ("Shiver Me Timbers!");
e.delete(6, 16);
// return "Shivers!"</code></pre>

#####11.1.4 The deleteCharAt Method
<pre><code>StringBuilder e = new StringBuilder ("Shiver Me Timbers!");
e.deleteCharAt(17);
// return "Shiver Me Timbers"</code></pre>

#####11.1.5 The reverse Method
<pre><code>StringBuilder e = new StringBuilder ("part");
e.reverse();
// return "trap"</code></pre>

####11.2 Using Constructors of the StringBuilder Class
- *StringBuilder ()* : Constructs a string builder with no characters in it and an initial capacity of 16 characters
- *StringBuilder (CharSequence seq)* : Constructs a string builder that contains the same characters as the specified CharSequence.
- *StringBuilder (int capacity)* : Constructs a string builder with no characters in it and an initial capacity specified by the capacity argument
- *StringBuilder (String str)* : Constructs a string builder initialized to the contents of the specified string

##CERTIFICATION OBJECTIVE 12: Test Equality Between Strings and Other Objects

###Exam Objective
- Test equality between strings and other objects using == and equals()

###Learning Outcome
- equals method in a class must override the equals method of *Object* class
- hashcode method also needs to be overridden
- String class overrides the equals method

<pre><code>/** The value is used for character storage. */
private final char value[];
/** The offset is the first index of the storage that is used.*/
private final int offset;
...
public boolean equals(Object anObject) {
	if (this == anObject){
		return true;
	}
	if (anObject instanceof String){
		String anotherString = (String) anObject;
		int n = count;
		if (n == anotherString.count) {
			char v1[] = value;
			char v2[] = anotherString.value;
			int i = offset;
			int j = anotherString.offset;
			while (n-- != 0) {
				if (v1[i++] != v2[j++]){
					return false;
				}
			}
			return true;
		}
		return false;
	}
}</code></pre>

####12.1 equals Method of the String Class
- when comparing charater strings, use equals
- when comparing object references, use ==

####12.2 Working with the compareTo Method of the String Class
- Compares two strings lexicographically. The comparison is based on the Unicode value of each character in the strings. 
- The result is a negative integer if this String object lexicographically precedes the argument string. 
- The result is a positive integer if this String object lexicographically follows the argument string. The result is zero - if the strings are equal; compareTo returns 0 exactly when the equals(Object) method would return true.
- compareTo returns the difference of the two character values at position k in the two string -- that is, the value:
  - this.charAt(k)-anotherString.charAt(k)
 
- If there is no index position at which they differ, then the shorter string lexicographically precedes the longer string. In this case, compareTo returns the difference of the lengths of the strings -- that is, the value:
  - this.length()-anotherString.length()

##SELF TEST

###Understand Fundamental Operators
1.
2.
3.
4.
5.
6.
7.
8.
9.
10.

###Use String Objects and Their Methods
11.
12.
13.
14.
15.
16.

###Use StringBuilder Objects and Their Methods
17.

###TestEquality Between Strings and Other Objects
18.
19.

***
##Annexe

###Reading list
- [Java Operators](http://docs.oracle.com/javase/tutorial/java/nutsandbolts/operators.html)
- [Comparing Floating Point Numbers, 2012 Edition](http://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/)
- [FindBugs](http://findbugs.sourceforge.net/)
- [StringBuilder class](http://docs.oracle.com/javase/8/docs/api/java/lang/StringBuilder.html)
- [String class](http://docs.oracle.com/javase/8/docs/api/java/lang/String.html)