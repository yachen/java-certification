#PACKAGING, COMPILING, AND INTERPRETING JAVA CODE

##CERTIFICATION OBJECTIVE 1 : The Java Platform

###Exam Objective 
Compare and contrast the features and components of Java, such as platform independence, object orientation, encapsulation, and so on.

###Learning Outcome

####1.1 Platform Independence
- Execution of bytecode on JVM
- JVM -> Java Virtual Machine  
- bytecode -> compiled java code
- Java has a few different JVM specs for devices with different capabilities. All of these JVMs share a common Java core, but platform independence is limited to compatible versions.

####1.2 Object-Oriented
- Java is OO, C is procedural.

#####1.2.1 Encapsulation
The process that an OO language organizes related data and code together. The data and methods that are designed for internal use in the object are not exposed to others.

#####1.2.2 Abstraction
- The ablility to generalize algorithms 
- facilitate code reuse and flexibility
- inheritance and polymorphism are key concepts in creating resusable code (refer to CH7 and CH8)

#####1.2.3 Robust and Secure
- problem of C/C++ : misuse of pointers, memory management, buffer overruns
- Java was designed not to have explicit pointers. In C, pointers stores a memory address to an object.
- Memory management was addressed in Java with the JVM's built-in garbage collector. JVM periodically runs the GC, which looks for any objects that have gone out of scope or that are no longer referenced, and it automatically deallocates their memory.
- Buffer overruns are a common exploit vector found in software that does not check for them. Java automatically checks the bounds of arrays (throw exception if fail).

##CERTIFICATION OBJECTIVE 2 : Understand Packages

###Exam Objective
Import other Java packages to make them accesible in your code.

###Learning Outcome

####2.1 Package Design

- package name + class name = fully qualified class name
- Packaging promotes code reuse, maintainablility and the OO principle of encapsulation and modularity.
- Package Attribute Considerations

Package Attribute | Benefits of Applying the Package Attribute
------- | ----------------
Class coupling  | Package dependencies are reduced
System coupling  | Package dependencies are reduced 
Package size | Typically, larger packages support reusability, whereas smaller packages support maintainability
Maintainability | Often, software changes can be limited to a single package
Naming | convention, use a reverse domain name for the package structure, lowercase characters delimited with underscores

####2.2 Package and import Statements
 
#####2.2.1 The Package Statement
- valide package statements   

Package Statement | Related Directory Structure   
----------------- | ---------------------------
package java.net; | [directory_path]\java\net
package com.ocajexam.utilities; | [directory_path]\com\ocajexam\utilities
package package_name; | [directory_path]\package_name\

- optional
- limited to 1 per source file
- reverse the domain name of the organization or group creating the package
- names equate to directory structures
- names beginning with java.* and javax.* are reserved
- names should be lowercase, words separated by _

#####2.2.2 The Import Statement
- enables you to include source code from other classes into a source file at compile time
- valide import Statements

Import Statement | Definition
---------------- | ----------
import java.net.*; | Imports all the classes from the package java.net
import java.net.URL | Imports only the URL class from the package java.net
import static java.awt.Color.\*; | Imports all static members of the Color class of the package java.awt (J2SE 5.0 onward only)
import static java.awt.color.ColorSpace.CS_GRAY; | Imports the static member CS_GRAY of the ColorSpace calss of the package java.awt (J2SE 5.0 onward only)

- for maintenance purpose, it is better that you import your classes explicitly

Scenario | solution
-------- | --------
To paint basic graphics and images, which package? | AWT API, import java.awt.\*;
To use data streams, which package? |  Basic I/O, import java.io.\*;
To develop a networking application, which package? | Networking API, import java.net.\*;
To work with the collections framework, event model, and date/time facilities, which package? | Utilities API, import java.util.\*
To utilize the core Java classed and interfaces, which package? | core Java Language package which is imported by default, import java.lang.\*;

######2.2.2.1 The static import Statement
- introduced in Java SE 5.0

<pre><code>
/* Import static member ITALY */
import static java.util.Locale.ITALY;
... 
System.out.println("Local: " + ITALY); // prints "Local: it_IT"
...

/* Imports all static members in class Locale */   
import static java.util.Locale.*;
...
System.out.println("Local: " + ITALY); // prints "Local: it_IT"
System.out.println("Local: " + GERMANY); // prints "Local: de_DE"
System.out.println("Local: " + JAPANESE); // prints "Local: ja"
...
</code></pre>

- Replacing implicit import statements with explicit import statements => TestClass

####2.3 Understand Package-Derived Classes
- Oracle includes more than 200 packages in the Java SE 8 API
- Need know : Java utilities, basic i/o, networking, AWT (abstract window toolkit), Swing, and data/time

#####2.3.1 Java Utilities API
- package java.util
- Exam scope : *Java Collections Framework*, date and time facilities, internationalization, and some miscellaneous utility classes.
- Various Classes of the Java Collections Framework

Interface | Implementation | Description
--------- | -------------- | -----------
List | ArrayList, LinkedList, Vector | Data structures based on positional access
Map | HashMap, HashTable, LinkedHashMap, TreeMap | Data structures that map keys to values
Set | HashSet, LinkedHashSet, TreeSet | Data structures based on element uniqueness
Queue | PriorityQueue | Queues typically order elements in a FIFO manner. Priority queues order elements according to a supplied comparator.

- Collections API provides the Comparator interface to assist sorting where ordering is not natural
- java.lang package provides the Comparable interface to sort objects by their natural ordering

class name | brief introduction
---------- | ------------------
Date, Calendar, TimeZone | legacy date and time facilities
Locale | geographical regions
Currency | currencies per the ISO 4217 standard
Random | a random-number generator
StringTokenizer | breaks strings into tokens
Timer | provides a task scheduling facility

#####2.3.2 Java Basic Input/Output API
- package java.io

![java.io](img/io.jpg)

class name | brief introduction
---------- | ------------------
File | provides representation of file and dir pathnames
FileDescriptor |  provides means to function as handle for opening files and sockets
FilenameFilter | defines the functionality to filter filenames
RandomAccessFile | allows for the reading and writing of files to specified locations

> *not in exam scope*   
> In JDK 7, NIO.2 API was introduced in package java.nio
<pre><code>
// Print out .txt file names in a given folder
try {
  Files.walk(Paths.get("/opt/dnaProg/users/docs")).forEach(p -> {
	if (p.getFileName().toString().endsWith(".txt"))
	  System.out.println("Text doc:" + p.getFileName());
  });
} catch (IOException e) {
  e.printStackTrace();
}
</code></pre>

#####2.3.3 Java Networking API
- package java.net
![java.net](img/net.jpg)

#####2.3.4 Java AWT API
- package java.awt
- Java's original GUI API and has been superseded by Swing API
- AWT Heavyweight Component API : Swing is better
- AWT Focus subsystem : provides for navigation control between components

#####2.3.5 Java Swing API
- package javax.swing
- AWT classes prefaced with the addition of "J", Swing JButton, AWT Button
![javax.swing](img/swing.jpg)

scenario | solution
-------- | --------
create basic Java Swing components such as buttons, panes and dialog boxes | import javax.swing.*;
support text-related aspects of your Swing components | import javax.swing.text.\*;
implement and configure basic pluggable look-and-feel support | import javax.swing.plaf.\*;
use Swing event listeners and adapters | import javax.swing.event.\*;

- when common classes are separated into their own packages, code usablility and maintainability are enhanced
- Swing MVC

MVC | Representation
----- | -----
model | current state of each component
view | representation of the components on the screen
controller | the functionality that ties the UI components to events

#####2.3.6 JavaFX API
- package javafx
- FXML define UI, support CSS

##CERTIFICATION OBJECTIVE 3 : Understand Class Structure

###Exam Objective
Define the structure of a Java class

###Learning Outcome

####3.1 Naming Conventions
![Java Naming Conventions](img/naming.jpg)

####3.2 Separators and Other Java Source Symbols
![Symbols and Separators](img/simbols.jpg)

####3.3 Java Class Structure
<pre><code>
[modifiers] class classIdentifier [extends superClassIdentifier][implements interfaceIdentifier1, interfaceIdentifier2, etc.]{
	[data members]
	[constructors]
	[methods]
}
</code></pre>
- Each class may extend 1 and only 1 superclass
- Each class may implement 1 or more interfaces (seperated by commas)
- sample : SpaceShipSimulator

##CERTIFICATION OBJECTIVE 4 : Compile and Interpret Java Code

###Exam Objective
Create executable Java applications with a main method, run a program from the command line, including console output.

###Learning Outcome

####4.1 Java Compiler
- sample : GreetingsUniverse

#####4.1.1 Compiling Your Source Code
- Java compiler simply converts Java source files into bytecode.
- Syntax : <pre><code>javac [options] [source files]</code></pre>

#####4.1.2 Compiling Your Source Code with the -d Option
- To specify explicitly where you would like the compiled bytecode class files to go, use -d option. 
- If the source code was packaged, the bytecode will be placed into the relative subdirectories.
- Sample : <pre><code>javac -d classes GreetingsUnivers.java</code></pre>

#####4.1.3 Compiling Your Code with the -classpath Option
- To tell teh compiler where the desired classes and packages, use classpath option : -cp or -classpath
- sample
<pre><code>javac -d classes -cp 3rdPartyCode\classes\;. GreetingsUniverse.java</code></pre>
- No need to include the classpath option if 
	- the classpath is defined with the CLASSPATH environment variable
	- the desired files are in the present working directory
- On windows systems 
<pre><code>-classpath .;\dir_a\calsses_a\;\dir_b\classes_b\</code></pre>

- On POSIX-based systems
<pre><code>-classpath .:/dir_a/calsses_a/:/dir_b/classes_b/</code></pre>

- switches = command line parameters = command-line switches = options = flags
- To know complete set of switches : *java -help* or *javac -help*

####4.2 Java Interpreter
- syntax : <pre><code>java [-options] class [args...]</code></pre>

#####4.2.1 Interpreting Your Bytecode
- Bytecode conversion
![bytecode conversion](img/bytecode.jpg)

- On Microsoft Windows, to exclude the command window, use *javaw* command : <pre><code>javaw.exe MainClass</code></pre>
- On POSIX-based systems, use the ampersand to run the application as a background process : <pre><code>java MainClass &</code></pre>

#####4.2.2 Interpreting Your Code with the -classpath Option
- find classes at runtime by including *-cp* or *-classpath* option with the interpreter
- If the classes are packaged, then start the application by pointing the full path of the application to the base directory
<pre><code>java -cp classes com.ocajexam.tutorial.MainClass</code></pre>

#####4.2.3 Interpreting Your Bytecode with the -D Option
- To set new property values, use *-D* option
- or by instantiating the *Properties* class and use *setProperty* method 
- syntax : <pre><code>java -D[name]=[value] class</code></pre>
- sample : PropertiesManager.java
- Subset of System Properties
![Subset of System Properties](img/properties.jpg)

#####4.2.4 Retrieving the Version of the Interpreter with the -version Option
![-version command-line option](img/version.jpg)

<pre><code>javac -d class/ src/com/ocajexam/tutorial/planets/Earth.java
javac -d class/ src/com/ocajexam/tutorial/planets/Mars.java
javac -d class/ src/com/ocajexam/tutorial/planets/Venus.java
javac -d class/ -cp class/ src/com/ocajexam/tutorial/GreetingsUniverse.java
java -cp class/ com.ocajexam.tutorial.GreetingsUnivers</code></pre>

##SELF TEST
###Understanding Packages
1. A,B correct
2. D correct
3. B correct

###Understand Package-Derived Classes
4. D correct
5. C correct
6. C correct
7. C correct
8. D correct

###Understand Class Structure
9. A,B correct
10. A correct
11. B correct

###Compile and Interpret Java Code
12. D correct
13. A, C correct
14. A, C correct
15. A correct

***
##Annexe

###Reading list
- [Java SE API](http://docs.oracle.com/javase/8/docs/api/)
- [class Paths](https://docs.oracle.com/javase/7/docs/api/java/nio/file/Paths.html) since 1.7
- [class Files](https://docs.oracle.com/javase/7/docs/api/java/nio/file/Files.html) since 1.7
- [class IOException](https://docs.oracle.com/javase/7/docs/api/java/io/IOException.html) since 1.0
- [class DateTimeFormatter](https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html) since 1.8
- [class LocalDateTime](https://docs.oracle.com/javase/8/docs/api/java/time/LocalDateTime.html) since 1.8
- [class FileHandler](https://docs.oracle.com/javase/7/docs/api/java/util/logging/FileHandler.html) since 1.4
- [class SimpleFormatter](https://docs.oracle.com/javase/7/docs/api/java/util/logging/SimpleFormatter.html) since 1.4
- [class Logger](https://docs.oracle.com/javase/7/docs/api/java/util/logging/Logger.html) since 1.4
- [class Level](https://docs.oracle.com/javase/7/docs/api/java/util/logging/Level.html) since 1.4
- Swing: A Beginner’s Guide, by Herbert Schildt (McGraw-Hill Professional)
- Introducing JavaFX 8 Programming, by Herbert Schildt (Oracle Press)
- “[How to Write Unmaintainable Code](http://thc.org/root/phun/unmaintain.html),” by Roedy Green
- The Passionate Programmer: Creating a Remarkable Career in Software Development, by Chad Fowler

###Java Exception Mapping

error message | problem | solution
------------- | ------- | --------
cannot find symbol | unknown class | import the class
Error: Could not find or load main class TheClassName | it must be called with its fully-qualified name with package | must be called from the directory in which thepackagename exists
###Git Tricks

- To remove a file in GIT
<pre>
git rm file1.txt
git commit -m "remove file1.txt"
</pre>

###Vi Tricks

Description | command
----------- | -------
go to the beginning of a line | 0
go to the end of a line | $
copy one line | yy
paste | p
delete one line | dd
replacing every occurrence of a string in the entire text | :%s/pattern/replace/