import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.nio.file.Paths;
import java.nio.file.Files;

public class TestClass {
  public static void main(String[] args) throws IOException {
    /* Ensure directory has been created */
    Files.createDirectories(Paths.get("logs"));
    /* Get the date to be used in the filename */
    DateTimeFormatter df 
      = DateTimeFormatter.ofPattern("yyyyMMdd_hhmm");
    LocalDateTime now = LocalDateTime.now();
    String date = now.format(df);
    /* Set up the filename in the logs directory */
    String logFileName = "logs/testlog-" + date + ".txt";
    /* Set up Logger */
    FileHandler myFileHandler = new FileHandler(logFileName);
    myFileHandler.setFormatter(new SimpleFormatter());
    Logger ocajLogger = Logger.getLogger("OCAJ Logger");
    ocajLogger.setLevel(Level.ALL);
    ocajLogger.addHandler(myFileHandler);
    /* Log Message */
    ocajLogger.info("\nThis is a logged information message. ");
    /* Close the file */
    myFileHandler.close();
  }
}
