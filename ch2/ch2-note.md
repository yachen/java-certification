#PROGRMMING WITH JAVA STATEMENTS
![Java Statements](img/statement1.jpg)
![Java Statements](img/statement2.jpg)

- 4 main statement categories
	- Expression : used for the evaluation of expressions, include *assignment expression*
	- Conditional (decision) : assist in directing the flow of control when a decision needs to be made, include *if, if-then, if-then-else, switch*
	- Iteration : provide support in looping through blocks of code, include *for loop, enhanced for loop, while, do-while*
	- Transfer of control (seen within other types of statements) : provide a means of stopping or interrupting the normal flow of control, include *continue, break, return*

##CERTIFICATION OBJECTIVE 5 : Understand Assignment Statements

###Learning Outcome
- An assignement statement sets a value within a variable
- All assignment statements are considered to be *expression statements*
- Expressions in Java are anything that has a value or is reduced to a value
- Expression Statements
![expression](img/expression.jpg)

####5.1 The Assignment Expression Statement
- syntax <pre><code>variable = value;</code></pre>
- combined Statement   
![combined](img/combined.jpg)

##CERTIFICATION OBJECTIVE 6 : Create and Use Conditional Statements

###Exam Objective
- Create if and if/else and ternary contructs
- Use a switch statement

###Learning Outcome
- Conditional Statements   

![conditional](img/conditional.jpg)

####6.1 The if Conditional Statement
- If no braces, only one statement after if will be executed
- With braces (curly brackets), multiple enclosed statements (block) will be allowed for execution
- syntax   
<pre><code>if (expression)
    statementA;
else
    statementB;
</code></pre>

####6.2 The if-then Conditional Statement
- also known as *if else if* statement
- syntax
<pre><code>if (expressionA)
    statementA;
else if (expressionB)
    statementB;
</code></pre>

- The *if* statement can accept any expression that returns a boolean value, sample :
<pre><code>boolean b;
boolean bValue = (b = true); // Evaluate to true
if (bValue) System.out.println("TRUE");
else System.out.println("FALSE");
if (bValue = false) // Evaluate to false
  System.out.println("TRUE"); 
else System.out.println("FALSE");
if (bValue == false) System.out.println("TRUE");
else System.out.println("FALSE");

Output:

$ TRUE
$ FALSE
$ TRUE
</code></pre>

- the assignment statements of all primitives will return their primitive values, sample :
<pre><code>int i; // Valid declaration
int iValue = (i=1); // Valid evaluation to int
if (iValue) {}; // Fails here since a boolean value is expected in the expession
if (i=1) {} // same reason as above
// Error: incompatible types; found: int, required: boolean
</code></pre>

- An object form the Boolean wrappert class is also allowed, because it will go through unboxing.
- Unboxing is the automatic production of its primitive value in cases where it is needed, sample:
<pre><code>Boolean wrapperBoolean = new Boolean ("true");
boolean primitiveBoolean1 = wrapperBoolean.booleanValue(); // valid
boolean primitiveBoolean2 = wrapperBoolean; // valid because of unboxing
if (wrapperBoolean) System.out.println("Works because of unboxing");
</code></pre>

####6.3 The if-then-else Conditional Statement
- end with *else*
- syntax
<pre><code>if (expressionA)
	statementA;
else if (expressionB)
	statementB;
else if (expressionC)
	statementC;
...
else
	statementZZ;
</code></pre>

####6.4 The Ternary Operator
- variation of the *if-then-else* statement, also referred to as a conditional operator
- the only operator to use 3 operands
- syntax
<pre><code>result = testCondition ? value1 : value2</code></pre>
- great for checking and returning simple values

####6.5 The switch Conditional Statement
- when 2 case within the same *switch* have the same value, a compiler error will be thrown:
<pre><code>switch (intValue){
case 200: System.out.println("case 1");
// Compiler error, Error: duplicate case label
case 200: System.out.println("case 2");
}
</code></pre>
- the expression of *switch* must evaluate to *byte, short, int, or char*; wrapper classes are also allowed because of auto unbox.
- *enum* are permitted as well
- *String* are supported after Java SE 7
- scenario & solution
![switch](img/switch.jpg)

##CERTIFICATION OBJECTIVE 7 : Create and Use Iteration Statements

###Exam Objective
- compare loop constructs
- create and use for loops including the enhanced
- create and use while loops
- create and use do/while loops

###Learning Outcome
- Iteration Statements
![iteration](img/iteration.jpg)

####7.1 The for Loop Iteration Statement
- syntax
<pre>for ( initialization; expression; iteration) {
	// Sequence of statements
}<code>
</code></pre>
- declaring the init var in the for loop is allowed and is the common approache, however, you can't use the var once you have exited the loop

####7.2 The Enhanced for Loop Iteration Statement
- used to iterate through an array, a collection or an object that implements the interface *iterable*
- also knowas *each loop* or *for in loop* 
- syntax
<pre><code>for (type variable : collection) 
	statement-sequence</code></pre>
- exercise : iterating through an ArrayList while applying conditions => Fish.java

####7.3 The while Iteration Statement
- syntax
<pre><code>while (expression) {
	// Sequence of statements
}
</code></pre>

####7.4 The do-while Iteration Statement
- syntax
<pre><code>do {
	// Sequence of statements
} while (expression)
</code></pre>

####7.5 scenario & solution
![loop_scenario](img/loop_scenario.jpg)

####7.6 Knowing your Statement-Related Keywords
- Java Statement-Related Keywords
![statement_keywords](img/statement_keywords.jpg)

##CERTIFICATION OBJECTIVE 8 : Create and Use Transfer of Control Statements

###Exam Objective
- use breack and continue

###Learning Outcome

####8.1 The break Transfer of Control Statement
- *break* is used to exist or force an abrupt termination of the body of *switch, do, for, while, do-while* statements

####8.2 The continue Transfer of Control Statement
- *continue* is used to terminate the conrrent iteration of a *do, for, while, do-while* loop and continue with the next iteration

####8.3 The return Transfer of Control Statement
- *return* is used to exit a method and optionnally return a specified value as an expression.
- for void functions, *return* is optional

####8.4 The labeled Statement
-syntax
<pre><code>labelIdentifier:
	Statement (such as a for loop)
	...
	break labelIdentifier; // transfer to the end of labeled loop
	...
	continue labelIdentifier; // transfer to the beginning of labeled loop
</code></pre>

##SELF TEST 9/12 75%

###Understand Assignment Statements
1. D correct
2. B correct
3. D wrong -> A, C

###Create and Use Conditional Statements
4. C correct
5. A,B,C,D wrong -> A, B, C
6. F correct
7. A correct
8. C wrong -> A
9. C correct

###Create and Use Iteration Statements
10. A,B,C correct

###Create and Use Transfer of Control Statements
11. B correct
12. C correct

***
##Annexe

###Reading list
- The Java Language Specification: Java SE 8 Edition, by James Gosling, Bill Joy, Guy Steele, Gilad Bracha, and Alex Buckley (Oracle, 2015)
- Java Generics and Collections by Maurice Naftalin and Philip Wadler (O’Reilly, 2006)
- [class Float](https://docs.oracle.com/javase/7/docs/api/java/lang/Float.html)

###Java Exception Mapping

error message | problem | solution
------------- | ------- | --------
java.lang.RuntimeException: Uncompilable source code - incompatible types | save an invalid literal to a declared primitive type variable | variable type and value type should be the same
Error: variable m not found in class [ClassName] | used var not in scope | declare the var